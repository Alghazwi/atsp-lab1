async function set_malicious_cookie(cname, cvalue, exhours) {
    var d = new Date();
    d.setTime(d.getTime() + (exhours*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    location.reload();
}

async function deploy_malicious_page() {
    let evil_backend = 'http://localhost:3002/'

    var data = document.getElementById('maliciousPageCode').value;
    var url = evil_backend + 'api/set_malicious_page'
    data = btoa(data)

    var request = {
        method: 'POST',
        headers: {
            'Content-Type': 'text/plain'
        },
        body: data
    }
    fetch(url, request)
        .then(response => {
            if (response.ok) {
                document.getElementById("action-success").classList.remove('hidden');
                setTimeout(() => { document.getElementById("action-success").classList.add('hidden'); }, 1500);
            } else {
                document.getElementById("action-failure").classList.remove('hidden');
                setTimeout(() => { document.getElementById("action-failure").classList.add('hidden'); }, 1500);
            }
        })
}

async function deliver_malicious_page() {
    let evil_backend = 'http://localhost:3002/'
    var url = evil_backend + 'api/set_delivered'
    data = 'true'

    var request = {
        method: 'POST',
        headers: {
            'Content-Type': 'text/plain'
        },
        body: data
    }
    fetch(url, request)
        .then(response => {
            if (response.ok) {
                document.getElementById("action-success").classList.remove('hidden');
                setTimeout(() => { document.getElementById("action-success").classList.add('hidden'); }, 1500);
            } else {
                document.getElementById("action-failure").classList.remove('hidden');
                setTimeout(() => { document.getElementById("action-failure").classList.add('hidden'); }, 1500);
            }
        })
}

async function update_malicious_code() {
    const fetchSettings = {
        method: 'GET',
    };

    let evil_backend = 'http://localhost:3002'
    let url = evil_backend + '/api/malicious_page'
    let response = await fetch(url, fetchSettings)
    response = await response.json()
    if (response['text']) {
        if (response['text'] !== '') {
            document.getElementById('maliciousPageCode').value = atob(response['text']);
        } else {
            document.getElementById('maliciousPageCode').value = '<iframe height=80% width=80% src="http://localhost"></iframe>'
        }
    }
}
