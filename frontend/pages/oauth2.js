async function startAuth() {
    if (!checkAuth()) {
        const clientId = 'atsp-frontend';
        const redirectUri = 'http://localhost:3000/oauth/redirect';
        const authUrl = 'http://localhost:8200/oauth/authorize';
        const state = generateState('http://localhost/pages/oauth2.html');
        const scope = 'read';
        const type = 'code'
        const params = {
            client_id: clientId,
            redirect_uri: redirectUri,
            state: state,
            scope: scope,
            response_type: type
        }

        window.location.href = createRequest(authUrl, params)
    } else {
        document.cookie = 'access_token' +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        checkAuth();
    }
}

function checkAuth(){
    let cookie = getCookie('access_token');
    document.getElementById('login-logout').innerHTML = cookie ? 'Logout' : 'Login'
    return !!cookie
}

function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}

function createRequest(baseUrl, params) {
   return baseUrl + '?' + new URLSearchParams(params).toString();
}

function generateState(redirectUrl) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < 32; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result + ';redirectUri=' + redirectUrl;
}