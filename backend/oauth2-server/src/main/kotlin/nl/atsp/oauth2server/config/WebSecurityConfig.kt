package nl.atsp.oauth2server.config

import org.springframework.context.annotation.Bean
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.userdetails.User.withDefaultPasswordEncoder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.provisioning.InMemoryUserDetailsManager

@EnableWebSecurity
class WebSecurityConfig :
    WebSecurityConfigurerAdapter()
{
    private val users = listOf(
        "rowan",
        "robin",
        "mark",
        "edser"
    )

    override fun configure(http: HttpSecurity)
    {
        http
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER)
            .and()
            .authorizeRequests().antMatchers("/actuator/**").permitAll()
        super.configure(http)
    }

    @Bean
    override fun userDetailsService(): UserDetailsService
    {
        return InMemoryUserDetailsManager(generateUsers())
    }

    private fun generateUsers(): List<UserDetails>
    {
        return users.map {
            withDefaultPasswordEncoder()
                .username(it)
                .password("password")
                .roles("USER")
                .build()
        }
    }
}